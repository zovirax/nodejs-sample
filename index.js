const express = require('express');
const { appPort } = require('./config/app.config');
const setupRoutes = require('./api/routes/index');
const { preRoutes, postRoutes } = require('./api/middlewares/index');
const setupSchedulers = require('./schedulers/index');

const app = express();
preRoutes(app);
setupRoutes(app);
postRoutes(app);
setupSchedulers();

app.listen(appPort, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is listening to ${appPort}`);
});
