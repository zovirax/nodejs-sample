exports.up = knex => knex.schema
  .createTable('user_game_account', tableBuilder => {
    tableBuilder.integer('accountId')
      .references('id')
      .inTable('game_account');

    tableBuilder.integer('userId')
      .references('id')
      .inTable('user');

    tableBuilder.timestamp('createdAt').defaultTo(knex.fn.now());
  });

exports.down = knex => knex.schema
  .dropTableIfExists('user_game_account');
