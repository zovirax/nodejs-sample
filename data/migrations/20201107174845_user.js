exports.up = knex => knex.schema
  .createTable('user', tableBuilder => {
    tableBuilder.increments('id');

    tableBuilder.string('email', 64).notNullable().unique();
    tableBuilder.string('password', 60).notNullable();
    tableBuilder.string('fullName', 64).notNullable();

    tableBuilder.timestamp('createdAt').defaultTo(knex.fn.now());
    tableBuilder.timestamp('updatedAt');
  });

exports.down = knex => knex.schema
  .dropTableIfExists('user');
