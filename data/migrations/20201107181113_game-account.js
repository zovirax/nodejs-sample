exports.up = knex => knex.schema
  .createTable('game_account', tableBuilder => {
    tableBuilder.integer('id').primary();
    tableBuilder.string('nickname').unique();
  });

exports.down = knex => knex.schema
  .dropTableIfExists('game_account');
