exports.up = knex => knex.schema
  .createTable('game_statistics', tableBuilder => {
    tableBuilder.integer('accountId')
      .references('id')
      .inTable('game_account');

    tableBuilder.integer('battles');
    tableBuilder.integer('survivedBattles');
    tableBuilder.integer('wins');
    tableBuilder.integer('losses');
    tableBuilder.integer('xp');

    tableBuilder.timestamp('updatedAt').defaultTo(knex.fn.now());
  });

exports.down = knex => knex.schema
  .dropTableIfExists('game_statistics');
