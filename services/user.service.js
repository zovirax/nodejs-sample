const userQuery = require('../data/queries/user.query');
const bcrypt = require('../common/helpers/bcrypt.helper');
const { createError } = require('../common/errors/HttpError');
const { CHANGE_PASSWORD_FAILED, EMAIL_TAKEN } = require('../common/errors/error-code.json');
const { assert } = require('../common/helpers/throw.helper');

const changePassword = async ({ newPassword, oldPassword, id: userId }) => {
  const { password } = await userQuery.findById(userId);
  const isMatched = await bcrypt.compare(oldPassword, password);
  assert(isMatched, createError(CHANGE_PASSWORD_FAILED));
  await userQuery.changePassword({ password: newPassword, id: userId });
};

const updateProfile = async ({ email, ...userData }) => {
  const isEmailTaken = Boolean(await userQuery.findByEmail(email));
  assert(!isEmailTaken, createError(EMAIL_TAKEN));
  await userQuery.update({ email, ...userData });
  return { email, ...userData };
};

module.exports = {
  changePassword,
  updateProfile,
};
