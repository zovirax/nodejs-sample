const { client, ...connection } = require('./config/database.config');

const defaults = {
  client,
  connection,
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    directory: './data/migrations',
  },
};

module.exports = {
  development: defaults,
  production: defaults,
};
