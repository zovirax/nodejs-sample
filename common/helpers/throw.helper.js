const assert = (data = false, err) => {
  if (!data && err) {
    throw err;
  }
};

module.exports = {
  assert,
};
