const jwt = require('jsonwebtoken');
const { expiresIn, secretKey } = require('../../config/jwt.config');

module.exports = {
  createJwt: data => jwt.sign(data, secretKey, { expiresIn }),
  createDummyJwt: () => jwt.sign({}, secretKey),
  verifyJwt: token => jwt.verify(token, secretKey),
};
