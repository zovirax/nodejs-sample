const ms = require('ms');
const { expiresIn } = require('../../config/jwt.config');
const { isLocalHost, serverDomain: domain } = require('../../config/app.config');

const cookieOptions = {
  domain,
  httpOnly: !isLocalHost,
  secure: !isLocalHost,
  sameSite: isLocalHost ? 'Strict' : 'None',
};

const setAccessToken = (response, token) => {
  const maxAge = ms(expiresIn);
  response.cookie('accessToken', token, { maxAge, ...cookieOptions });
  return response;
};

const clearAccessToken = response => {
  response.clearCookie('accessToken');
  return response;
};

module.exports = {
  setAccessToken,
  clearAccessToken,
};
