const bcrypt = require('bcrypt');

module.exports = {
  hash: data => bcrypt.hash(data, 10),
  compare: (data, hash) => bcrypt.compare(data, hash),
};
