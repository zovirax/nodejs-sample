const axios = require('axios');
const { createError } = require('../errors/HttpError');
const { WARGAMING_API_REQUEST_FAILED } = require('../errors/error-code.json');

const WargamingBaseUrl = Object.freeze({
  WOT: 'https://api.worldoftanks.ru/wot',
});

const WargamingApiMethod = Object.freeze({
  GET: 'get',
  POST: 'post',
});

class WargamingClient {
  constructor(options) {
    const { applicationId, baseURL, language, timeout = 5000 } = options;
    this.applicationId = applicationId;
    this.language = language;
    this.client = axios.create({ baseURL, timeout });
  }

  async request(method = WargamingApiMethod.GET, url = '', params = {}) {
    const { applicationId, language } = this;

    const { data: { status, data, error } } = await this.client[method](url, {
      params: { application_id: applicationId, language, ...params },
    });

    if (status === 'ok') {
      return data;
    }

    const { message } = error;
    throw createError({ ...WARGAMING_API_REQUEST_FAILED, message });
  }

  get(url, params) {
    return this.request(WargamingApiMethod.GET, url, params);
  }

  post(url, params) {
    return this.request(WargamingApiMethod.POST, url, params);
  }
}

module.exports = {
  WargamingBaseUrl,
  WargamingApiMethod,
  WargamingClient,
};
