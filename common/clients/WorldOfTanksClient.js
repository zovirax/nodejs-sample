const { WargamingClient, WargamingBaseUrl } = require('./WargamingClient');
const { wotApplicationId: applicationId } = require('../../config/wargaming.confg');

class WorldOfTanksClient extends WargamingClient {
  constructor(options = {}) {
    super({ applicationId, baseURL: WargamingBaseUrl.WOT, ...options });
  }

  async getAccountByNickname(search) {
    const [res] = await this.get('/account/list/', { search, type: 'exact' });
    const { account_id: accountId, ...account } = res || {};
    return accountId && { accountId, ...account };
  }

  async getAccountStatistics(accountId) {
    const res = (await this.get('/account/info/', { account_id: accountId }))[accountId];
    if (!res) return null;
    const { statistics: { all } } = res;
    const { survived_battles: survivedBattles, ...stats } = all;
    return { survivedBattles, ...stats };
  }
}
module.exports = WorldOfTanksClient;
