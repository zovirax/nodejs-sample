const { Router } = require('express');
const { changePasswordValidation, updateProfileValidation } = require('../middlewares/validation/user.validation');
const userService = require('../../services/user.service');

const router = Router();

router.patch('/password', changePasswordValidation, (req, res, next) => {
  userService.changePassword({ ...req.body, ...req.user })
    .then(() => res.sendStatus(204))
    .catch(next);
});

router.put('/', updateProfileValidation, (req, res, next) => {
  userService.updateProfile({ ...req.body, id: req.user.id })
    .then(user => res.ok({ user }))
    .catch(next);
});

module.exports = router;
