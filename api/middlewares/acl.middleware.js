const { routes, Role } = require('../../config/acl.config');
const errorCode = require('../../common/errors/error-code.json');
const { createError } = require('../../common/errors/HttpError');

module.exports = (req, res, next) => {
  const { originalUrl } = req;

  const { permissions: requiredPermissions = [] } = routes.find(({ resource }) => {
    const [pattern, isNoPattern = true] = resource.split('/**');
    return isNoPattern ? resource === originalUrl : originalUrl.includes(pattern);
  }) || {};

  const { roles: requiredRoles = [] } = requiredPermissions.find(({ method }) => req.method === method) || {};
  const { role: userRole, id: userId } = req.user;

  const isAllowed = role => (role === userRole || role === Role.ALL || (role === Role.AUTHORIZED && userId)
    || (role === Role.ANONYMOUS && !userId));

  // eslint-disable-next-line no-nested-ternary
  const error = !requiredPermissions.length ? errorCode.NOT_FOUND
    : (userId ? errorCode.FORBIDDEN : errorCode.UNAUTHORIZED);

  return requiredRoles.some(isAllowed)
    ? next()
    : next(createError(error));
};
