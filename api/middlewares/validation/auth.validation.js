const { emailRules, passwordRules, fullNameRules } = require('./shared.mixin');
const validator = require('./index');

const registerRules = [
  ...emailRules,
  ...passwordRules,
  ...fullNameRules,
];

const loginRules = [
  ...emailRules,
  ...passwordRules,
];

module.exports = {
  registerValidation: [...registerRules, validator],
  loginValidation: [...loginRules, validator],
};
