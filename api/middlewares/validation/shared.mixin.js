const { body, param, query } = require('express-validator');

const emailRules = [
  body('email')
    .trim()
    .isEmail()
    .normalizeEmail({ gmail_remove_dots: false })
    .isLength({ max: 64 }),
];

const passwordRules = [
  body('password')
    .isLength({
      min: 6,
      max: 32,
    }),
];

const fullNameRules = [
  body('fullName')
    .trim()
    .isLength({ min: 1, max: 32 })
    .escape(),
];

const idRules = [
  param('id')
    .isNumeric()
    .notEmpty(),
];

module.exports = {
  emailRules,
  passwordRules,
  fullNameRules,
  idRules,
};
