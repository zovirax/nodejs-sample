const { body } = require('express-validator');
const { emailRules, fullNameRules } = require('./shared.mixin');
const validator = require('./index');

const changePasswordRules = [
  body('newPassword')
    .isLength({
      min: 6,
      max: 32,
    }),
  body('oldPassword')
    .isLength({
      min: 6,
      max: 32,
    }),
];

const updateProfileRules = [
  ...emailRules,
  ...fullNameRules,
];

module.exports = {
  changePasswordValidation: [changePasswordRules, validator],
  updateProfileValidation: [updateProfileRules, validator],
};
