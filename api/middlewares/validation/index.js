const { validationResult } = require('express-validator');
const { createError } = require('../../../common/errors/HttpError');
const { VALIDATION } = require('../../../common/errors/error-code.json');

module.exports = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const invalidParams = errors.array().map(({ param }) => param).join(', ');
    const isPlural = errors.array().length > 1;
    const message = `${invalidParams} ${isPlural ? 'are' : 'is'} not valid`;
    throw createError({ ...VALIDATION, message });
  }
  next();
};
