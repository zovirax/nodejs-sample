#### Get started
npm install && npm run db:migrate && npm run start
  
#### Technologies
- Node.js/Express/ES6+
- REST API
- PostgreSQL
- Passport.js
- knex.js (SQL query builder and migrations)
- Wargaming API
