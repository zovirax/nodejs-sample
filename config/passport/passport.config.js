const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const bcrypt = require('../../common/helpers/bcrypt.helper');
const { jwtOptions } = require('./passport-jwt.config');
const userQuery = require('../../data/queries/user.query');
const { createError } = require('../../common/errors/HttpError');
const errorCode = require('../../common/errors/error-code.json');

passport.use(
  'register',
  new LocalStrategy(
    {
      usernameField: 'email',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        const userByEmail = await userQuery.findByEmail(email);
        if (userByEmail) {
          return done(createError(errorCode.EMAIL_TAKEN), null);
        }
        return done(null, {
          email,
          password: await bcrypt.hash(password, 10),
          fullName: req.body.fullName,
        });
      } catch (err) {
        return done(err);
      }
    },
  ),
);

passport.use('login',
  new LocalStrategy({
    usernameField: 'email',
  }, async (email, password, done) => {
    try {
      const userByEmail = await userQuery.findByEmail(email);
      if (!userByEmail) {
        return done(createError(errorCode.EMAIL_WRONG), null);
      }
      const isMatched = await bcrypt.compare(password, userByEmail.password);
      return (isMatched ? done(null, userByEmail) : done(createError(errorCode.PASSWORD_WRONG), null));
    } catch (err) {
      return done(err);
    }
  }));

passport.use('jwt', new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
  try {
    const { sub: id } = jwtPayload;
    const userById = id ? (await userQuery.findById(id) || {}) : {};
    done(null, userById);
  } catch (err) {
    done(err);
  }
}));
