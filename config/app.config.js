require('dotenv').config();

const { PORT, NODE_ENV, SERVER_DOMAIN } = process.env;

module.exports = {
  appPort: PORT,
  env: NODE_ENV,
  domain: SERVER_DOMAIN,
  isLocalHost: SERVER_DOMAIN === 'localhost',
};
