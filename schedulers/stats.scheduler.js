const { CronJob } = require('cron');
const { updateActualStats } = require('../services/game.service');

// Job runs every 2 minutes
new CronJob('*/2 * * * *', () => updateActualStats()
  .catch(console.error))
  .start();
